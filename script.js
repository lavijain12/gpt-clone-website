// JavaScript to handle adding new chats
const newChatButton = document.getElementById("new-chat-button");
const listOfChats = document.getElementById("list-of-chats");

newChatButton.addEventListener("click", () => {
    // Prompt the user for a chat title (you can replace this with your own logic)
    const chatTitle = prompt("Enter chat title:");
    if (chatTitle) {
        // Create a new chat item and append it to the list
        const chatItem = document.createElement("div");
        chatItem.classList.add("single-chat-item");

        const chatIcon = document.createElement("div");
        chatIcon.classList.add("chat-icon");

        const chatTitleDiv = document.createElement("div");
        chatTitleDiv.classList.add("chat-title");
        chatTitleDiv.textContent = chatTitle;

        chatItem.appendChild(chatIcon);
        chatItem.appendChild(chatTitleDiv);

        listOfChats.appendChild(chatItem);
    }
});

// JavaScript to handle sending messages
const sendButton = document.getElementById("send-button");
const searchInput = document.getElementById("search-input");

sendButton.addEventListener("click", () => {
    const messageText = searchInput.value;
    if (messageText) {
        // Replace this with your logic to send the message
        alert("Message sent: " + messageText);

        // Clear the input field after sending
        searchInput.value = "";
    }
});
